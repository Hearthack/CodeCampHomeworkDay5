const assert = require('assert');
const fs = require("fs");

const myFunction = require("./homework5-1.js");
const checkFileExist = myFunction.checkFileExist;
const readFileAndJSONParse = myFunction.readFileAndJSONParse;

describe('tdd lab', function() {
  describe('#checkFileExist()', function() {

        it('should have homework5-1_eyes.json existed', async () => {
            const accessEyes = await checkFileExist("homework5-1_eyes.json");
            assert.strictEqual (accessEyes,true, "Can not access file homework5-1_eyes.json");
        });
        
        it('should have homework5-1_gender.json existed', async () => {
            const accessGender = await checkFileExist("homework5-1_gender.json");
            assert.strictEqual (accessGender,true, "Can not access file homework5-1_gender.json");
        });

        it('should have homework5-1_friends.json existed', async () => {
            const accessFriends = await checkFileExist("homework5-1_friends.json");
            assert.strictEqual (accessFriends,true, "Can not access file homework5-1_friends.json");
        });
        
    });

    describe('#objectKey()', function() {

        it('should have same object key stucture as homework5-1_eyes.json', async () => {
            let eyesKeys = ["brown","blue","green"];
            let eyesData = await readFileAndJSONParse("homework5-1_eyes.json");
            assert.deepEqual(Object.keys(eyesData), eyesKeys, 'Eyes Keys is not Equal');
        });
        
        it('should have same object key stucture as homework5-1_friends.json', async () => {
            let friendsKeys = ['_id','friendCount'];
            let friendsData = await readFileAndJSONParse("homework5-1_friends.json");
            assert.deepEqual(Object.keys(friendsData[0]), friendsKeys, 'Friends Keys is not Equal');
        });
        
        it('should have same object key stucture as homework5-1_gender.json', async () => {
            let genderKeys = ['male','female'];
            let genderData = await readFileAndJSONParse("homework5-1_gender.json");
            assert.deepEqual(Object.keys(genderData), genderKeys, 'Gender Keys is not Equal');
        });
        
    });

    describe('#userFriendCount()', function() {
        it('should have size of array input as 23', async () => {
                let friendsData = await readFileAndJSONParse("homework5-1_friends.json");
                assert.deepEqual(friendsData.length, 23, 'Friends all is not 23')
        });
    });

    describe('#sumOfEyes()', function() {
        it('should have sum of eyes as 23', async () => {
            let eyesData = await readFileAndJSONParse("homework5-1_eyes.json");
            let sum = Object.values(eyesData)
                .reduce((total,data)=>{
                    return total+data;
                },0);
            assert.deepEqual(sum, 23, 'Eyes all is not 23')
        });
    });

    describe('#sumOfGender()', function() {
        it('should have sum of gender as 23', async () => {
            let genderData = await readFileAndJSONParse("homework5-1_gender.json");
            let sum = Object.values(genderData)
            .reduce((total,data)=>{
                return total+data;
            });
            assert.deepEqual(sum, 23, 'Gender all is not 23')       
        });
    });

});