const fs = require("fs");

let checkFileExist = (filename) =>  {
    return new Promise((resolve, reject) => { 
        fs.access(filename, (err) => {
            if (err) 
                reject(err); 
            else 
                resolve(true); 
        });
    }); 
}

let readFileAndJSONParse = (filename) =>  {
    return new Promise((resolve, reject) => { 
        fs.readFile(filename, 'utf8',(err,data) => { 
            if (err) 
                reject(err); 
            else
                //console.log("Read file" + " " + filename + " " + "complete."); 
                resolve(JSON.parse(data)); 
        });
    }); 
}

let writeFileAndJSONstringify = (filename,data) => {
    return new Promise((resolve,reject) => {
        fs.writeFile(filename,JSON.stringify(data),'utf8', (err) => {
            if(err)
                reject(err); 
            else
                //console.log("Write file" + " " + filename + " " + "complete.");
                resolve(true);          
        });
    });
}

function countEyeColor(data){
    return new Promise ((resolve,reject) => {
        try { 
            let eyeColors = data.map(item => item.eyeColor);
            let result = {"brown":0,"blue":0,"green":0};
            eyeColors.forEach(value =>{
                result[value] += 1;
            });
            resolve(result);
        }catch(err){
            reject(err);
        }
    });
}

function countGender(data){
    return new Promise ((resolve,reject) => {
        try { 
            let gender = data.map(item => item.gender);
            let result = { male: 0, female: 0 };
            gender.forEach(value =>{
                result[value] += 1;
            });
            resolve(result);
        }catch(err){
            reject(err);
        }
    });
}

function countFriends(data){
    return new Promise ((resolve,reject) => {
        try { 
            const friends = data.map(item => {
                return {'_id' : item._id ,'friendCount' : item.friends.length};
            });
            resolve(friends);
        }catch(err){
            reject(err);
        }
    });
}

async function runMyFunction() {
    try {  
        let data = await readFileAndJSONParse("homework1-4.json"); 
        let dataEyeColor  = await countEyeColor(data);
        await writeFileAndJSONstringify("homework5-1_eyes.json",dataEyeColor);
        let dataGender = await countGender(data);
        await writeFileAndJSONstringify("homework5-1_gender.json",dataGender);
        let dataFriends = await countFriends(data);
        await writeFileAndJSONstringify("homework5-1_friends.json",dataFriends);
    } catch (error) {
        console.error(error); 
    } 
} 
runMyFunction();

exports.checkFileExist = checkFileExist;
exports.readFileAndJSONParse = readFileAndJSONParse;